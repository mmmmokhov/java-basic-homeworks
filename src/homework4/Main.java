package homework4;

public class Main {
    public static void main(String[] args) {
        Pet pet1 = new Pet("Cat", "Skotik", 5, 85, new String[]{"Sleep", "eat", "srat"});
        Pet pet2 = new Pet("Dog", "Pesel", 3, 55, new String[]{"Walk", "play", "run"});
        Human nadiia = new Human("Nadiia", "Mokhova", 1989, 99, "Lubov Syn", "Volodymyr Syn", pet2);
        Human oleksandr = new Human("Oleksandr", "Mokhov", 1989, 75, "Olena Tes", "Andrii Tes", pet1);
        String[][] schedule;

        pet1.printMe();
        pet1.eat();
        pet1.foul();
        pet1.respond();
        nadiia.printMe();
        oleksandr.printMe();
        nadiia.greetPet();
    }
}
