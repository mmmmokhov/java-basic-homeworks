package homework4;

import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String mother;
    private String father;
    private Pet pet;


    Human(String name, String surname, int year, int iq, String mother, String father, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.pet = pet;
    }

    Human(String name, String surname, int year, int iq, String mother, String father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
    }

    Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    Human(){}

    void greetPet() {
        System.out.printf("Привет, %s", pet);
    }
//    void describePet() {
//
//        if (trickLevel > 50) {
//        String trick;
//            trick == "очень хитрый";
//        } else {
//            trick == "почти не хитрый";
//        }
//        System.out.printf("У меня есть %s, ему %s лет, он [очень хитрый]/[почти не хитрый", species, age, );
//    }

    @Override
    public String toString() {
        return String.format("Human{name='%s', surname='%s', year='%d', iq=%d, mother=%s, father=%s, pet=%s", name, surname, year, iq, mother, father, pet);
    }

    void printMe() {
        System.out.println(this);
    }
}
