package homework4;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    Pet(){}


    void eat() {
        System.out.println("Я кушаю");
    }
    void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", nickname);
    }
    void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age='%d', trickLevel='%d', habits=%s}\n", species, nickname, age, trickLevel, Arrays.toString(habits));
    }

    void printMe() {
        System.out.println(this);
    }
}
