package homework1;

import java.util.Random;
import java.util.Scanner;

public class ArraysNumbers {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Let the game begin!");
        System.out.println("Enter your name: ");
        String userName = in.nextLine();
        int randomNumber = random.nextInt(101);


        while (true) {
            System.out.println("Enter a number between 0 and 100: ");
            while (!in.hasNextInt()) {
                System.out.println("Please, enter a correct integer number between 0 and 100: ");
                in.next();
            }
            int userNumber = in.nextInt();
            if (userNumber < randomNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (userNumber > randomNumber) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.printf("Congratulations, %s!", userName);
                break;
            }
        }
        in.close();
    }
}
