package homework2;

import java.util.Random;
import java.util.Scanner;

public class SeaBattle {

    //    Заполнение поля
    public static void fillBoard(String[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = " - ";
            }
        }
    }

    //    Вывод поля в консоль
    public static void printBoard(String[][] board) {
        for (int i = 0; i < board.length + 1; i++) {
            System.out.printf(" %d |", i);
        }
            System.out.println();
        for (int y = 0; y < board.length; y++) {
            System.out.printf(" %d |", (y + 1));

            for (int x = 0; x < board[y].length; x++) {
                System.out.printf("%s|", board[y][x]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void main(String[] args) {
//        задаем размер поля
        int rows = 5;
        int cols = 5;

        String[][] board = new String[rows][cols];
        fillBoard(board);

//      установка корабля
        Random random = new Random();
        int targetRow = random.nextInt(rows);
        int targetCol = random.nextInt(cols);

        System.out.println("All set. Get ready to rumble!\n");

        while (true) {

            printBoard(board);

//      запрос предполагаемых координат для выстрела
            Scanner scanner = new Scanner(System.in);
            System.out.printf("Enter a row number (1-%d): ", rows);
            int guessRow = scanner.nextInt() - 1;
            System.out.printf("Enter a column number (1-%d): ", cols);
            int guessCol = scanner.nextInt() - 1;

//      проверка введенных данных
            if (guessRow < 0 || guessRow >= rows || guessCol < 0 || guessCol >= cols) {
                System.out.println("No, that's not even in the field.\n");
            } else if (board[guessRow][guessCol] == " * ") {
                System.out.println("You guessed that one already.\n");
            } else {
                // Проверка на попадание в цель
                if (guessRow == targetRow && guessCol == targetCol) {
                    System.out.println("You have won!\n");
                    board[targetRow][targetCol] = " x ";
                    break;
                } else {
                    System.out.println("You missed, try again!\n");
                    board[guessRow][guessCol] = " * ";
                }
            }
        }

        printBoard(board);

    }
}
