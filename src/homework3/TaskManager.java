package homework3;

import java.util.Arrays;
import java.util.Scanner;

public class TaskManager {
    public static void main(String[] args) {
        enum Day {
            SUNDAY,
            MONDAY,
            TUESDAY,
            WEDNESDAY,
            THURSDAY,
            FRIDAY,
            SATURDAY
        }

        String[][] schedule = new String[7][2];

        for (int i = 0; i < Day.values().length; i++) {
            schedule[i][0] = Day.values()[i].name();
        }

        schedule[0][1] = "do home work";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][1] = "java courses";
        schedule[3][1] = "rest";
        schedule[4][1] = "do home work 4";
        schedule[5][1] = "drink alcohol";
        schedule[6][1] = "play with son";

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Please, input the day of the week:");
            String userDay = scanner.nextLine().toLowerCase().trim();

            String task = "";
            switch (userDay) {
                case "sunday":
                    task = schedule[0][1];
                    break;
                case "monday":
                    task = schedule[1][1];
                    break;
                case "tuesday":
                    task = schedule[2][1];
                    break;
                case "wednesday":
                    task = schedule[3][1];
                    break;
                case "thursday":
                    task = schedule[4][1];
                    break;
                case "friday":
                    task = schedule[5][1];
                    break;
                case "saturday":
                    task = schedule[6][1];
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    continue;
            }
            System.out.printf("Your tasks for %s: %s\n", userDay, task);
        }
    }
}
